FROM gcc:10

LABEL maintainer="robert.soor@gmail.com"

# Get packages
RUN apt update -q && apt install -y cmake git libgles2-mesa-dev libxt-dev libxaw7-dev libsdl2-dev libzzip-dev libfreeimage-dev libfreetype6-dev libpugixml-dev

# Clone and prepare Ogre
RUN cd ~/ && \
    git clone https://github.com/leggedrobotics/ogre && \
    cd ogre && \
    git checkout raisimOgre && \
	mkdir build && \
	cd build && \
	cmake .. -DCMAKE_BUILD_TYPE=Release -DOGRE_BUILD_COMPONENT_BITES=ON -OGRE_BUILD_COMPONENT_JAVA=OFF -DOGRE_BUILD_DEPENDENCIES=OFF -DOGRE_BUILD_SAMPLES=False

# Build and install Ogre
RUN cd ~/ogre/build && \
    make install -j8

ENTRYPOINT ["/bin/bash"]
