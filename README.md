# Docker - gcc-Ogre #

The Dockerfile to a custom Docker container. It is an extension of the official gcc image, including:

 * Installed Ogre renderer
 * cmake tools
 * git

This container was created because building Ogre takes a very long time. So including it in the container saves a lot of build minutes.

A fork of the official Ogre is installed: https://github.com/leggedrobotics/ogre

## Build ##

Build the container with something like:

```
docker build -t ogre .
```

## hub.docker.com ##

The container is hosted at: https://hub.docker.com/r/be1et/gcc-ogre

Containers are built automatically from this repository.
